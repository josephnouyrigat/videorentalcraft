﻿using System.Collections;
using System.Linq;
using System.Text;

namespace VideoStore
{
    public class Customer
    {
        private readonly ArrayList Rentals = new ArrayList();
        private string Name { get; }

        public Customer(string name)
        {
            Name = name;
        }

        public void AddRental(Rental rental)
        {
            Rentals.Add(rental);
        }

        public string Statement()
        {
            decimal totalAmount = 0;
            int frequentRenterPoints = 0;
            StringBuilder sb = new StringBuilder("Rental Record for ");
            sb.Append(Name)
            sb.AppendLine();
            foreach (var rental in Rentals.Cast<Rental>())
            {
                // add frequent renter points
                frequentRenterPoints++;
                // add bonus for a two day new release rental
                if ((rental.Movie.PriceCode == MovieType.NewRelease) && rental.DaysRented > 1)
                    frequentRenterPoints++;

                // show figures for this rental
                decimal amount = getAmount(rental);
                sb.Append("\t");
                sb.Append(rental.Movie.Title);
                sb.AppendLine();
                sb.Append("\t");
                sb.Append(amount:F1);
                sb.AppendLine();
                totalAmount += amount;
            }

            // add footer lines
            sb.Append("Amount owed is ");
            sb.Append(totalAmount:F1);
            sb.AppendLine();
            sb.Append("You earned ");
            sb.Append(frequentRenterPoints);
            sb.Append(" frequent renter points");
            sb.AppendLine();
            return sb.ToString();
        }

        private decimal getAmount(Rental rental) {
            switch (rental.Movie.PriceCode) {
                case MovieType.Regular:
                    if (each.DaysRented > 2)
                        return 2 + (each.DaysRented - 2) * 1.5m;
                    else
                        return 2;
                case MovieType.NewRelease:
                    return each.DaysRented * 3;
                case MovieType.Children:
                    if (each.DaysRented > 3)
                        return 1.5m + (each.DaysRented - 3) * 1.5m;
                    else
                        return 1.5m;
                default:
                    return 0;
            }
        }
    }
}