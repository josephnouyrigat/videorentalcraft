﻿namespace VideoStore
{
    public class Movie
    {
        private string Title { get; }
        private MovieType PriceCode { get; set; }

        public Movie(string title, int priceCode)
        {
            Title = title;
            PriceCode = priceCode;
        }
    }
}