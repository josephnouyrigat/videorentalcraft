﻿namespace VideoStore
{
    public class Rental
    {
        private Movie Movie { get; }
        private int DaysRented { get; }

        public Rental(Movie movie, int daysRented)
        {
            Movie = movie;
            DaysRented = daysRented;
        }
    }
}