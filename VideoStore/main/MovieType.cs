public enum MovieType : int {
    Regular = 0,
    NewRelease = 1,
    Children = 2
}